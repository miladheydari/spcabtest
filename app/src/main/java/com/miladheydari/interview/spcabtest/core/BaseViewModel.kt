package com.miladheydari.interview.spcabtest.core

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()
