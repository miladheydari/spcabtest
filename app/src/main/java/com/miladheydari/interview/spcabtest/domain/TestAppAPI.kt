package com.miladheydari.interview.spcabtest.domain


import com.miladheydari.interview.spcabtest.domain.model.VehicleList
import com.miladheydari.interview.spcabtest.core.Constants
import io.reactivex.Single
import retrofit2.http.GET

interface TestAppAPI {

    @GET(Constants.NetworkService.VEHICLES_LIST)
    fun getVehicles(): Single<VehicleList>


}
