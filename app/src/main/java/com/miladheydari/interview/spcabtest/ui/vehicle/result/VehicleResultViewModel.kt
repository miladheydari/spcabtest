package com.miladheydari.interview.spcabtest.ui.vehicle.result

import androidx.databinding.ObservableField
import com.miladheydari.interview.spcabtest.core.BaseViewModel
import com.miladheydari.interview.spcabtest.domain.model.Vehicle

import javax.inject.Inject

public class VehicleResultViewModel @Inject internal constructor() : BaseViewModel() {
    var item = ObservableField<Vehicle>()
}