package com.miladheydari.interview.spcabtest.domain.usecase

import androidx.lifecycle.LiveData
import androidx.lifecycle.map
import com.miladheydari.interview.spcabtest.domain.model.Vehicle
import com.miladheydari.interview.spcabtest.repo.VehicleRepository

import com.miladheydari.interview.spcabtest.ui.vehicle.VehicleViewState
import com.miladheydari.interview.spcabtest.utils.UseCaseLiveData
import com.miladheydari.interview.spcabtest.utils.domain.Resource
import javax.inject.Inject

class VehicleUseCase @Inject internal constructor(
    private val repository: VehicleRepository
) : UseCaseLiveData<VehicleViewState, VehicleUseCase.VehicleParams,
        VehicleRepository>() {

    override fun getRepository(): VehicleRepository {
        return repository
    }

    override fun buildUseCaseObservable(params: VehicleParams?): LiveData<VehicleViewState> {
        return repository.loadSource(
            fetchRequired = params?.fetchRequired ?: true
        ).map {
            onCurrentVehicleResultReady(it)
        }
    }

    private fun onCurrentVehicleResultReady(resource: Resource<List<Vehicle>>): VehicleViewState {
        return VehicleViewState(
            status = resource.status,
            error = resource.error,
            data = resource.data
        )
    }

    data class VehicleParams(
        val fetchRequired: Boolean
    )
}
