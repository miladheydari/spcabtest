package com.miladheydari.interview.spcabtest.repo


import androidx.lifecycle.LiveData
import com.miladheydari.interview.spcabtest.domain.datasource.VehicleLocalDataSource
import com.miladheydari.interview.spcabtest.domain.datasource.VehicleRemoteDataSource
import com.miladheydari.interview.spcabtest.domain.model.Vehicle
import com.miladheydari.interview.spcabtest.domain.model.VehicleList
import com.miladheydari.interview.spcabtest.utils.domain.Resource
import io.reactivex.Single
import javax.inject.Inject

class VehicleRepository @Inject constructor(
    private val vehicleRemoteDataSource: VehicleRemoteDataSource,
    private val vehicleLocalDataSource: VehicleLocalDataSource
) {


    fun loadSource(fetchRequired: Boolean): LiveData<Resource<List<Vehicle>>> {
        return object :
            NetworkBoundResource<List<Vehicle>, VehicleList>() {
            override fun saveCallResult(item: VehicleList) {

                vehicleLocalDataSource.insert(item.vehicles)
            }

            override fun shouldFetch(data: List<Vehicle>?): Boolean {
                return fetchRequired
            }


            override fun loadFromDb(): LiveData<List<Vehicle>> {
                return vehicleLocalDataSource.get()
            }

            override fun createCall(): Single<VehicleList> {
                return vehicleRemoteDataSource.get()
            }

            override fun onFetchFailed() {
            }

        }.asLiveData
    }
}
