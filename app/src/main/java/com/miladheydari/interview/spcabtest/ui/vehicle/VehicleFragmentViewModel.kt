package com.miladheydari.interview.spcabtest.ui.vehicle

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.switchMap
import com.miladheydari.interview.spcabtest.core.BaseViewModel
import com.miladheydari.interview.spcabtest.domain.usecase.VehicleUseCase


import javax.inject.Inject

class VehicleFragmentViewModel @Inject internal constructor(
    private val vehicleUseCase: VehicleUseCase
) : BaseViewModel() {


    private val _vehicleParams: MutableLiveData<VehicleUseCase.VehicleParams> = MutableLiveData()

    fun getSourceViewState() = currentVehicleViewState

    private val currentVehicleViewState: LiveData<VehicleViewState> =
        _vehicleParams.switchMap { params ->
            vehicleUseCase.execute(params)
        }

    fun setSourceParams(params: VehicleUseCase.VehicleParams) {
        if (_vehicleParams.value == params)
            return
        _vehicleParams.postValue(params)
    }


}
