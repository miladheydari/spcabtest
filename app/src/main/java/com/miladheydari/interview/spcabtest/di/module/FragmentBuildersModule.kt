package com.miladheydari.interview.spcabtest.di.module

import com.miladheydari.interview.spcabtest.ui.vehicle.MapsFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentBuildersModule {


    @ContributesAndroidInjector
    abstract fun contributeVehicleFragment(): MapsFragment
}
