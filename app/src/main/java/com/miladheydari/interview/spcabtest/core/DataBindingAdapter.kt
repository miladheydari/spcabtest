package com.miladheydari.interview.spcabtest.core

import android.view.View
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.miladheydari.interview.spcabtest.R
import com.miladheydari.interview.spcabtest.utils.GlideApp

import com.miladheydari.interview.spcabtest.utils.extensions.hide
import com.miladheydari.interview.spcabtest.utils.extensions.show

@BindingAdapter("app:visibility")
fun setVisibility(view: View, isVisible: Boolean) {
    view.hide()
    if (isVisible) {
        view.show()
    } else {
        view.hide()
    }
}

@BindingAdapter("app:setDrawableLink")
fun setDrawableLink(view: ImageView, link: String?) {
    if (link.isNullOrEmpty())
        return
    GlideApp.with(view.context)
        .load(link)
        .diskCacheStrategy(DiskCacheStrategy.AUTOMATIC)
        .into(view)
}