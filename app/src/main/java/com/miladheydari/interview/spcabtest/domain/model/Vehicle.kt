package com.miladheydari.interview.spcabtest.domain.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName


@Entity(tableName = "vehicle", primaryKeys = ["lat", "lng", "type", "bearing"])
data class Vehicle(
    @SerializedName("type")
    var type: String = "",
    @SerializedName("lat")
    var lat: Double = 0.0,
    @SerializedName("lng")
    var lng: Double = 0.0,
    @SerializedName("bearing")
    var bearing: Int = 0,
    @SerializedName("image_url")
    var imageUrl: String = ""
)

