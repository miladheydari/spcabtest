package com.miladheydari.interview.spcabtest.domain.datasource


import com.miladheydari.interview.spcabtest.domain.TestAppAPI
import com.miladheydari.interview.spcabtest.domain.model.VehicleList

import io.reactivex.Single
import javax.inject.Inject

class VehicleRemoteDataSource @Inject constructor(private val api: TestAppAPI) {

    fun get(): Single<VehicleList> {
        return api.getVehicles()
    }
}
