package com.miladheydari.interview.spcabtest.ui.vehicle

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.miladheydari.interview.spcabtest.R
import com.miladheydari.interview.spcabtest.core.BaseFragment
import com.miladheydari.interview.spcabtest.databinding.FragmentMapsBinding
import com.miladheydari.interview.spcabtest.di.Injectable
import com.miladheydari.interview.spcabtest.domain.model.Vehicle
import com.miladheydari.interview.spcabtest.domain.usecase.VehicleUseCase
import com.miladheydari.interview.spcabtest.ui.vehicle.result.SourceResultAdapter
import com.miladheydari.interview.spcabtest.utils.GlideApp
import com.miladheydari.interview.spcabtest.utils.RotateTransformation
import com.miladheydari.interview.spcabtest.utils.extensions.getBitmap
import com.miladheydari.interview.spcabtest.utils.extensions.observeWith

class MapsFragment :
    BaseFragment<VehicleFragmentViewModel, FragmentMapsBinding>(VehicleFragmentViewModel::class.java),
    Injectable {

    private var gMap: GoogleMap? = null
    private var vehicles: List<Vehicle>? = null
    private val callback = OnMapReadyCallback { googleMap ->
        gMap = googleMap
        vehicles?.let { initMap(it) }
    }


    override fun init() {
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync(callback)

        initAdapter()
        setViewModelObserver()

    }


    private fun initAdapter() {
        val adapter = SourceResultAdapter()
        val layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        mBinding.recyclerView.adapter = adapter
        mBinding.recyclerView.layoutManager = layoutManager


    }


    private fun initVehicleResultAdapter(list: List<Vehicle>) {
        (mBinding.recyclerView.adapter as SourceResultAdapter).submitList(list)

    }


    private fun setViewModelObserver() {
        viewModel.setSourceParams(getParams())

        viewModel.getSourceViewState().observeWith(viewLifecycleOwner) {
            mBinding.viewState = it
            it.data?.let { results ->
                vehicles = results

                vehicles?.let { v ->
                    initMap(v)
                    initVehicleResultAdapter(v)
                }
            }


        }
    }

    private fun initMap(vehicles: List<Vehicle>) {
        gMap?.let { map ->
            map.clear()
            if (vehicles.isNotEmpty()) {
                vehicles.forEach { vehicle ->

                    GlideApp.with(this).asBitmap().load(vehicle.imageUrl)
                        .transform(RotateTransformation(vehicle.bearing.toFloat()))
                        .getBitmap {
                            it?.let {
                                map.addMarker(
                                    MarkerOptions().position(LatLng(vehicle.lat, vehicle.lng)).icon(
                                        BitmapDescriptorFactory.fromBitmap(it)
                                    ).title(vehicle.type)
                                )
                                map.moveCamera(
                                    CameraUpdateFactory.newLatLngZoom(
                                        LatLng(
                                            vehicle.lat,
                                            vehicle.lng
                                        ), 15f
                                    )
                                )
                            }
                        }
                }
            }
        }
    }


    private fun getParams(): VehicleUseCase.VehicleParams {
        return VehicleUseCase.VehicleParams(
            fetchRequired = true
        )
    }


    override fun getLayoutRes() = R.layout.fragment_maps

    override fun initViewModel() {

    }
}