package com.miladheydari.interview.spcabtest.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.miladheydari.interview.spcabtest.di.ViewModelFactory
import com.miladheydari.interview.spcabtest.di.key.ViewModelKey
import com.miladheydari.interview.spcabtest.ui.main.MainActivityViewModel
import com.miladheydari.interview.spcabtest.ui.vehicle.VehicleFragmentViewModel

import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory
    @IntoMap
    @Binds
    @ViewModelKey(MainActivityViewModel::class)
    abstract fun provideMainActivityViewModel(mainActivityViewModel: MainActivityViewModel): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VehicleFragmentViewModel::class)
    abstract fun provideSourceFragmentViewModel(sourceFragmentViewModel: VehicleFragmentViewModel): ViewModel
}
