package com.miladheydari.interview.spcabtest.ui.vehicle

import com.miladheydari.interview.spcabtest.domain.model.Vehicle
import com.miladheydari.interview.spcabtest.utils.domain.Status


class VehicleViewState(
    val status: Status,
    val error: Throwable? = null,
    val data: List<Vehicle>? = null
) {


    fun isLoading() = status == Status.LOADING

    fun shouldShowErrorMessage() = error != null
}
