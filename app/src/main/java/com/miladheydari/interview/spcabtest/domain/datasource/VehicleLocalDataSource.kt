package com.miladheydari.interview.spcabtest.domain.datasource

import com.miladheydari.interview.spcabtest.db.dao.VehicleDao

import com.miladheydari.interview.spcabtest.domain.model.Vehicle

import javax.inject.Inject

class VehicleLocalDataSource @Inject constructor(private val vehicleDao: VehicleDao) {

    fun get() = vehicleDao.get()

   
    fun insert(vehicles: List<Vehicle>) = vehicleDao.insertReplace(vehicles)

}
