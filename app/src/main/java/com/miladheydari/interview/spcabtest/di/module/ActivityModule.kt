package com.miladheydari.interview.spcabtest.di.module


import com.miladheydari.interview.spcabtest.di.scope.PerActivity
import com.miladheydari.interview.spcabtest.ui.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityModule {

    @PerActivity
    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    internal abstract fun mainActivity(): MainActivity
}
