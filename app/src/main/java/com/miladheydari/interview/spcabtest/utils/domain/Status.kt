package com.miladheydari.interview.spcabtest.utils.domain

// references :
// https://developer.android.com/jetpack/docs/guide#addendum

enum class Status {
    SUCCESS,
    LOADING,
    ERROR
}
