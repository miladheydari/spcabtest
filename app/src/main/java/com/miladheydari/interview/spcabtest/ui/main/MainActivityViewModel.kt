package com.miladheydari.interview.spcabtest.ui.main

import com.miladheydari.interview.spcabtest.core.BaseViewModel
import javax.inject.Inject

class MainActivityViewModel @Inject internal constructor() : BaseViewModel()