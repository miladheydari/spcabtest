package com.miladheydari.interview.spcabtest.db.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.miladheydari.interview.spcabtest.domain.model.Vehicle


@Dao
interface VehicleDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertReplace(vehicles: List<Vehicle>)

    @Query("SELECT * FROM vehicle")
    fun get(): LiveData<List<Vehicle>>




    @Transaction
    fun deleteAndInsert(vehicles: List<Vehicle>) {
        delete()
        insertReplace(vehicles)
    }

    @Query("DELETE FROM vehicle")
    fun delete()
}