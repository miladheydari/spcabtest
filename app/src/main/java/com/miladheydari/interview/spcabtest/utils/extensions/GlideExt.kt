package com.miladheydari.interview.spcabtest.utils.extensions

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.miladheydari.interview.spcabtest.utils.GlideRequest


fun GlideRequest<Bitmap>.getBitmap(callback: (Bitmap?) -> (Unit)) {
    this.into(object : SimpleTarget<Bitmap>() {
        override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
            callback.invoke(resource)
        }

        override fun onLoadFailed(errorDrawable: Drawable?) {
            callback.invoke(null)
        }

    })

}
