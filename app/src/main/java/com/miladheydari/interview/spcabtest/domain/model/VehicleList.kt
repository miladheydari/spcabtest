package com.miladheydari.interview.spcabtest.domain.model

import com.google.gson.annotations.SerializedName


data class VehicleList(
    @SerializedName("vehicles") val vehicles: List<Vehicle>
    )