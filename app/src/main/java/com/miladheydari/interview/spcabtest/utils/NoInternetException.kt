package com.miladheydari.interview.spcabtest.utils

import java.io.IOException

class NoInternetException(s: String) : IOException(s)
