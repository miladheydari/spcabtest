package com.miladheydari.interview.spcabtest.ui.vehicle.result

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.DiffUtil
import com.miladheydari.interview.spcabtest.core.BaseAdapter
import com.miladheydari.interview.spcabtest.databinding.ItemVehicleBinding
import com.miladheydari.interview.spcabtest.domain.model.Vehicle


class SourceResultAdapter : BaseAdapter<Vehicle>(diffCallback) {

    override fun createBinding(parent: ViewGroup, viewType: Int): ViewDataBinding {
        val mBinding = ItemVehicleBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        val viewModel = VehicleResultViewModel()
        mBinding.viewModel = viewModel


        return mBinding
    }

    override fun bind(binding: ViewDataBinding, position: Int) {
        (binding as ItemVehicleBinding).viewModel?.item?.set(getItem(position))
        binding.executePendingBindings()
    }
}

val diffCallback = object : DiffUtil.ItemCallback<Vehicle>() {
    override fun areContentsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean =
        oldItem == newItem

    override fun areItemsTheSame(oldItem: Vehicle, newItem: Vehicle): Boolean =
        oldItem.lat == newItem.lat &&
                oldItem.lng == newItem.lng &&
                oldItem.type == newItem.type &&
                oldItem.bearing == newItem.bearing
}
