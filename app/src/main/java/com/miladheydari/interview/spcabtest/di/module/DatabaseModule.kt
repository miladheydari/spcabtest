package com.miladheydari.interview.spcabtest.di.module

import android.content.Context
import com.miladheydari.interview.spcabtest.db.AppDatabase

import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun getDatabase(context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    @Singleton
    fun providesVehicleDao(db: AppDatabase) = db.vehicleDao()

}
